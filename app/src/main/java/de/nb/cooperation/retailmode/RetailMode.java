package de.nb.cooperation.retailmode;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import de.nb.cooperation.retailmode.admin.DemoDeviceAdminReceiver;
import de.nb.cooperation.retailmode.service.RetailModeService;


public class RetailMode extends Activity {

	// RetailModeService instanziieren
    RetailModeService rms=new RetailModeService();

	int cur=0;
	boolean last=false;

    ViewPager viewPager;
	CustomSwipeAdapter adapter;
    // BroadcastReceiver zum Beenden des retailmodes
	private final BroadcastReceiver abcd = new BroadcastReceiver() {
         @Override
         public void onReceive(Context context, Intent intent) {
               finish();                                   
         }
     };
	
    // Activation für DeviceAdmin 
    static final int ACTIVATION_REQUEST = 47; // identifies our request id
 	DevicePolicyManager devicePolicyManager;
 	ComponentName demoDeviceAdmin;
	
 	// Variable für ClassName zur logausgabe
 	private static final String TAG = RetailMode.class.getSimpleName();

    // 3 Secunden als Intervall
    private final int interval = 5000; // 1 Second
	private final int interval2 = 8000; // 1 Second
    //TimeroutHandler
    private Handler handler = new Handler();
	private Handler handler2 = new Handler();
    // Variable zum Erfassen eines "Longpressed
    private Boolean pressed=false;
    // Runnable zum Prüfen auf "Longpressed und Start der entsprechenden Activity
    private Runnable runnable = new Runnable(){
        public void run() {
        	if (pressed==true) {
	        	Intent i = new Intent();
	            i.setClassName("de.nb.cooperation.retailmode", "de.nb.cooperation.retailmode.security.AdminLogin");
	            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		            
	            startActivity(i);
	            pressed=false;
            }   
        }
    };
	private Runnable runnable2 = new Runnable(){
		public void run() {
			if (viewPager.getCurrentItem() + 1 >= adapter.getCount() && cur == viewPager.getCurrentItem()) {
				viewPager.setCurrentItem(0);
			} else {
				viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
			}
			handler2.postAtTime(runnable2, System.currentTimeMillis()+interval2);
			handler2.postDelayed(runnable2, interval2);
		}
	};


    /**
     * OnCreate Methode wird bei Instanziierung ausgeführt
     */
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// verschiede App-Parameter setzen
		getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|WindowManager.LayoutParams.FLAG_FULLSCREEN|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);

        // PowerManager setzen (Bildschirm soll eingeschaltet bleiben)
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();
        wl.release();

        // Video starten
        startPlayback();
        // Receiver registrieren
        registerReceiver(abcd, new IntentFilter("xyz"));

        // Initialize Device Policy Manager service and our receiver class
     	devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
     	demoDeviceAdmin = new ComponentName(this, DemoDeviceAdminReceiver.class);

     	// Activate device administration
     	Intent intent = new Intent(
		DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
		intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
				demoDeviceAdmin);
		intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
				"Schutz vor Deinstallation");
		startActivityForResult(intent, ACTIVATION_REQUEST);
		handler2.postAtTime(runnable2, System.currentTimeMillis()+interval2);
		handler2.postDelayed(runnable2, interval2);
	}

	/**
	 * Methode zum prüfen ob Gerät ausgeschaltet werden soll. Dieses Verhindern
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {		
	    super.onWindowFocusChanged(hasFocus);
	    if(!hasFocus) {	        
	        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
	        sendBroadcast(closeDialog);
	    }
	}

	/**
	 * Methode bei Fortsetzen der App
	 * Start des Videos
	 */
	@Override
	public void onResume() {
   	    super.onResume();
	    startPlayback();
	}

	public WakeLock getWakeLock() {
		return null;		
	}
	
	/**
	 * Methode zum STarten des Videos
	 */

	 public void startPlayback() {
	    // Ein View setzen (hier videoplayer)
        setContentView(R.layout.videoplayer);
		final TextView text1= ( TextView ) findViewById(R.id.swipe1);
		final TextView text2= ( TextView ) findViewById(R.id.swipe2);
		final TextView text3= ( TextView ) findViewById(R.id.swipe3);
		final ImageView[] images = {( ImageView) findViewById(R.id.pager1),
				( ImageView) findViewById(R.id.pager2),
				( ImageView) findViewById(R.id.pager3),
				( ImageView) findViewById(R.id.pager4),
				( ImageView) findViewById(R.id.pager5),( ImageView) findViewById(R.id.pager6)};

		viewPager= (ViewPager)findViewById(R.id.view_pager);
		adapter = new CustomSwipeAdapter(this);

	    viewPager.setAdapter(adapter);
		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){


			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {

			}

			@Override
			public void onPageScrollStateChanged(int state){
				int[][] swipe = {{R.string.swipe_1_1,R.string.swipe_1_2,R.string.swipe_1_3},
						{R.string.swipe_2_1,R.string.swipe_2_2,R.string.swipe_2_3},
						{R.string.swipe_3_1,R.string.swipe_3_2,R.string.swipe_3_3},
						{R.string.swipe_4_1,R.string.swipe_4_2,R.string.swipe_4_3},
						{R.string.swipe_5_1,R.string.swipe_5_2,R.string.swipe_5_3},
						{R.string.swipe_6_1,R.string.swipe_6_2,R.string.swipe_6_3}};


				text1.setText(swipe[viewPager.getCurrentItem()][0]);
				text2.setText(swipe[viewPager.getCurrentItem()][1]);
				text3.setText(swipe[viewPager.getCurrentItem()][2]);

				handler2.removeCallbacks(runnable2);
				handler2.postAtTime(runnable2, System.currentTimeMillis()+interval2);
				handler2.postDelayed(runnable2, interval2);

				for(int i=0; i < images.length; i++) {
					images[i].setImageResource((viewPager.getCurrentItem()!=i)?R.drawable.point:R.drawable.point_active);
				}

				if (state == ViewPager.SCROLL_STATE_IDLE && last==false) {
					if (viewPager.getCurrentItem() + 1 >= adapter.getCount() && cur == viewPager.getCurrentItem()) {
						last=true;
						viewPager.setCurrentItem(0);
					}
					if(viewPager.getCurrentItem() == 0 && cur == 0) {
						last=true;
						viewPager.setCurrentItem(adapter.getCount()-1);
					}

					cur = viewPager.getCurrentItem();
				}
				else if (state == ViewPager.SCROLL_STATE_IDLE && last==true) {
					last=false;
				}
			}

		});

        /*ImageView left=(ImageView)findViewById(R.id.left) ;
		 ImageView right=(ImageView)findViewById(R.id.right) ;
		 right.setOnTouchListener(new View.OnTouchListener(){

			 @Override
			 public boolean onTouch(View view, MotionEvent event) {
				 String str2="ACTION_UP";
				 if(str2.equals(event.actionToString(event.getAction()))) {
					 if (viewPager.getCurrentItem() + 1 >= adapter.getCount() && cur == viewPager.getCurrentItem()) {

						 viewPager.setCurrentItem(0);
					 } else {
						 viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
					 }
				 }
				 return true;
			 }
		 });
		 left.setOnTouchListener(new View.OnTouchListener(){

			 @Override
			 public boolean onTouch(View view, MotionEvent event) {
				 String str2="ACTION_UP";
				 if(str2.equals(event.actionToString(event.getAction()))) {
					 if (viewPager.getCurrentItem() == 0 && cur == 0) {

						 viewPager.setCurrentItem(adapter.getCount() - 1);
					 } else {
						 viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
					 }
				 }
				 return true;
			 }
		 });*/


        // Video heraussuchen (für View)
        //videoView = (VideoView)findViewById(R.id.video);
        //Log.d(TAG,String.valueOf(videoView.getWidth())+"test");
        
    
        // Url zusammenbasteln
        //String uriPath = "android.resource://de.nb.cooperation.retailmode/"+R.raw.video;
        //Uri uri = Uri.parse(uriPath);
        // Url setzen
        //videoView.setVideoURI(uri);
        //videoView.requestFocus();
        //videoView.setLongClickable(true);

        // Looping einstellen
        /*videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        // Video starten
        //videoView.start();




        /**
         * Touchlistener auf Video (zum EInstigf in Admin)
         **/



        ImageView logo = ( ImageView ) findViewById(R.id.vid_logo);
        logo.setOnTouchListener(new View.OnTouchListener(){
        	@Override
        	public boolean onTouch(View v, MotionEvent event) {
        		// Action down und Up prüfen
        		String str="ACTION_DOWN";

        		// Timer und Videoplaying auf 0/false setzen
        		rms.videoIsPlaying=false;
        		rms.timer=0;
        		// Starten des prüfrunnables ob Longpressed
				if(str.equals(event.actionToString(event.getAction()))) {
					handler.postAtTime(runnable, System.currentTimeMillis()+interval);
					handler.postDelayed(runnable, interval);
					pressed=true;
				}        			
				// Wenn kurz gepresst, Video stoppen udn activity schließen
				/*else if(str2.equals(event.actionToString(event.getAction()))) {        		
        		    pressed=false;
        		    finish();
        		    
        		}*/
        		return true;
        	}
        });
         ImageView testbuttton = (ImageView)findViewById(R.id.entdecken_view);
         testbuttton.setOnTouchListener(new View.OnTouchListener(){

             @Override
             public boolean onTouch(View view, MotionEvent event) {
                 String str2="ACTION_DOWN";
                 if(str2.equals(event.actionToString(event.getAction()))) {
                     pressed = false;
                     finish();
					 Intent startMain = new Intent(Intent.ACTION_MAIN);
					 startMain.addCategory(Intent.CATEGORY_HOME);
					 startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					 startActivity(startMain);
                 }
                 return false;
             }
         });
        // Videoplaying auf true setzen
        rms.videoIsPlaying=true;
    }

	 /**
	  * Methode beim Zerstören der Activity
	  */
	 @Override
	 public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Alles unregister und beednen sowie Variablen zurück setzen
		unregisterReceiver(abcd);
		finish();
		rms.videoIsPlaying=false;
		rms.timer=0;
	 };

	 /**
	  * methode beim PAsusieren der App
	  */
	 @Override
	 public void onPause() {
		super.onPause();
		// Variablen Zurücksetzen
		rms.videoIsPlaying=false;
		rms.timer=0;
	 }

	/**
	 * Called when startActivityForResult() call is completed. The result of
	 * activation could be success of failure, mostly depending on user okaying
	 * this app's request to administer the device.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ACTIVATION_REQUEST:
			if (resultCode == Activity.RESULT_OK) {
				Log.d(TAG, "Administration enabled!");
				startService(new Intent(this, RetailModeService.class));

			} else {
				Log.d(TAG, "Administration enable FAILED!");
			}
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}