package de.nb.cooperation.retailmode.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import de.nb.cooperation.retailmode.RetailMode;

public class BootReceiver extends BroadcastReceiver {
	 private static final String TAG = RetailMode.class.getSimpleName();

	  @Override
	  public void onReceive(Context context, Intent intent) {
		 if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
		    Intent myIntent = new Intent(context, RetailMode.class);
		    myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    context.startActivity(myIntent);
		 }
	  }
	}