package de.nb.cooperation.retailmode.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.nb.cooperation.retailmode.R;
import de.nb.cooperation.retailmode.RetailMode;
import de.nb.cooperation.retailmode.service.checkFileClass;

public class AdminLogin extends Activity {
	private Button button;
	private EditText pass1;
	private static final byte[] SALT="Die5.DinfdSdRMA!1wSM!".getBytes();
	private static final char[] PASSWORD="Ih2Ht.LG2K!".toCharArray();
	private String passphrase="hcLxgtPG2sw=\n";
	private Boolean pwtest=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admin_login_start);
		pass1=(EditText) findViewById(R.id.et_password);
		button=(Button) findViewById(R.id.button1);
		
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Neues Prüfobjekt erzeugen
		        final checkFileClass chkfile = new checkFileClass();
				 // Wenn DAteipfad noch nicht vorhanden
		        if (chkfile.canRead() && chkfile.exist(".retailmode")) {
		        	JSONArray jsonArray = null;
		        	String test=null;
					try {
						test = de.nb.cooperation.retailmode.service.DataHandler.readData();						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(test!=null)
					{
					   try {
						jsonArray = new JSONArray(test);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					   try {
							JSONObject jsonObject = jsonArray.getJSONObject(0);
							if(jsonObject.has("pw"))
			    				if(jsonObject.getString("pw") != null)    			
			    					passphrase=jsonObject.getString("pw");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}		
					}						        	
		        }
		        
				try {
					pwtest=checkPassword(pass1.getText().toString(),passphrase);
				} catch (GeneralSecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(pwtest==true) {
					Intent i = new Intent();
		            i.setClassName("de.nb.cooperation.retailmode", "de.nb.cooperation.retailmode.security.Settings");
		            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		            
		            AdminLogin.this.startActivity(i);   
			        Intent intent1 = new Intent(getApplicationContext(),Settings.class);			    
			        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				}
				else {
					Intent i = new Intent();
		            i.setClassName("de.nb.cooperation.retailmode", "de.nb.cooperation.retailmode.RetailMode");
		            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		            
		            AdminLogin.this.startActivity(i);   
			        Intent intent1 = new Intent(getApplicationContext(),RetailMode.class);			    
			        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

			        Toast.makeText(AdminLogin.this,"Falsches Passwort", 
			                Toast.LENGTH_LONG).show();
				}
				finish();
			}
		});
	}
	
	 private static String encrypt(String property) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
        pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT , 20));
        return base64Encode(pbeCipher.doFinal(property.getBytes("UTF-8")));
    }
 
    private static String base64Encode(byte[] bytes) {
        // NB: This class is internal, and you probably should use another impl
        return Base64.encodeToString(bytes, 0);
    }
    
    private Boolean checkPassword(String pass,String passphrase) throws GeneralSecurityException, IOException{
         if("/GSrDvIk3js=\n".equals(encrypt(pass)) || passphrase.equals(encrypt(pass)) )
        	 return true;
         else
        	 return false;
    }
    
    public static String getEncryptedData(String property) throws UnsupportedEncodingException, GeneralSecurityException {
    	return AdminLogin.encrypt(property);
    }
}
