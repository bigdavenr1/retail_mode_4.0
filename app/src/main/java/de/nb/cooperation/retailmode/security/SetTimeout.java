package de.nb.cooperation.retailmode.security;

import org.json.JSONArray;
import org.json.JSONException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.nb.cooperation.retailmode.R;
import de.nb.cooperation.retailmode.service.DataHandler;

public class SetTimeout extends Activity {
	private JSONArray jsonArray=null;
    final static DataHandler dataHandler = new DataHandler(); 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 try {
	        	jsonArray=dataHandler.readInfos();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		
		setContentView(R.layout.settimeout);
		final EditText standbyTime = (EditText)findViewById(R.id.editText3);
		final Button setTimeOut = (Button)findViewById(R.id.button2);		
		standbyTime.setText(Integer.toString(dataHandler.standByStart));	
		
		setTimeOut.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				try {
					if(!String.valueOf(standbyTime.getText()).equals("")){
						dataHandler.writeOut("standby",String.valueOf(standbyTime.getText()),SetTimeout.this);
						startActivity(new Intent(SetTimeout.this,Settings.class));
					}
					else{
						Toast.makeText(SetTimeout.this,"Bitte Wert eingeben (0 für Steuerung mit StandBy)", 
				                Toast.LENGTH_SHORT).show(); 	
					}					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}