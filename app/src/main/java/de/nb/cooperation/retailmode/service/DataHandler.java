package de.nb.cooperation.retailmode.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

public class DataHandler {
	// Neues Prüfobjekt erzeugen
    final static checkFileClass chkfile = new checkFileClass();
    private static JSONArray jsonArray=null;
	
    public static Boolean Mon=true;
	public static Boolean Tue=true;
	public static Boolean Wed=true;
	public static Boolean Thu=true;
	public static Boolean Fri=true;
	public static Boolean Sat=true;
	public static Boolean Sun=false;
	public static int startHour = 10;
	public static int startMinute = 0;
	public static int stopHour = 20;
	public static int stopMinute = 0;
	public static int standByStart = 4;	
	public static int dayOfWeek=0;
	
	public String getTime(String typ)
	{
        Date now = new Date();

        SimpleDateFormat format = null;
        if(typ.equals("wd"))
            format = new SimpleDateFormat("E",Locale.US);
        if(typ.equals("h"))
            format = new SimpleDateFormat("H",Locale.US);
        if(typ.equals("m"))
            format = new SimpleDateFormat("m",Locale.US);

       return format.format(now);
	}
	
	 /**
	    * Methode zum Auslesen der DAten
	 * @return 
    * @throws InterruptedException
    * @throws JSONException
    */
 	public JSONArray readInfos() throws InterruptedException, JSONException {
 		boolean settingsfile = false;
 		
		if (chkfile.canRead() && chkfile.exist(".retailmode")) {
    		String[] test=chkfile.readDirectory(".retailmode");
   			if(test.length>=1) settingsfile=true; 			    		
    	}
    	if (settingsfile==true)	{    	
    		JSONArray jsonArray = new JSONArray(readData());
    		for (int i = 0; i < jsonArray.length(); i++) {
    			JSONObject jsonObject = jsonArray.getJSONObject(i);
    			if(jsonObject.has("standby"))
    				if(jsonObject.getString("standby") != null)    			
    					standByStart=Integer.parseInt(jsonObject.getString("standby"));    			
    			if(jsonObject.has("vonH"))
	    			if(jsonObject.getString("vonH") != null)    			
	    				startHour=Integer.parseInt(jsonObject.getString("vonH"));
    			if(jsonObject.has("vonM"))
    				if(jsonObject.getString("vonM") != null)    			
    					startMinute=Integer.parseInt(jsonObject.getString("vonM"));
    			if(jsonObject.has("bisH"))
    				if(jsonObject.getString("bisH") != null)    			
    					stopHour=Integer.parseInt(jsonObject.getString("bisH"));
    			if(jsonObject.has("bisM"))
    				if(jsonObject.getString("bisM") != null)    			
    					stopMinute=Integer.parseInt(jsonObject.getString("bisM"));
    			if(jsonObject.has("Mon"))
    				if(jsonObject.getString("Mon") != null && !jsonObject.getString("Mon").equals("0"))    			
    					Mon=true;
    				else
    					Mon=false;
    			if(jsonObject.has("Tue"))
    				
    				if(jsonObject.getString("Tue") != null && !jsonObject.getString("Tue").equals("0"))    			
    					Tue=true;    					    				
    				else
    					Tue=false;    			
    			if(jsonObject.has("Wed"))
    				if(jsonObject.getString("Wed") != null && !jsonObject.getString("Wed").equals("0"))    			
    					Wed=true;
    				else
    					Wed=false;
    			if(jsonObject.has("Thu"))
    				if(jsonObject.getString("Thu") != null && !jsonObject.getString("Thu").equals("0"))    			
    					Thu=true;
    				else
    					Thu=false;
    			if(jsonObject.has("Fri"))
    				if(jsonObject.getString("Fri") != null && !jsonObject.getString("Fri").equals("0"))    			
    					Fri=true;
    				else
    					Fri=false;
    			if(jsonObject.has("Sat"))
    				if(jsonObject.getString("Sat") != null && !jsonObject.getString("Sat").equals("0"))    			
    					Sat=true;
    				else
    					Sat=false;
    			if(jsonObject.has("Sun"))
    				if(jsonObject.getString("Sun") != null && !jsonObject.getString("Sun").equals("0"))    			
    					Sun=true;
    				else
    					Sun=false;
			}
    		return jsonArray;
    	}
		return null;
 	}
 	
    // Funktion zum Auslesen der Daten aus der DAtei
 	public static String readData() throws InterruptedException {  		
 		String test = chkfile.read(".retailmode/", "data.mem");
 		if(test != null)
 			return test.toString();
		return null; 		
 	}
 	
 	public Boolean getState()
 	{
 		Boolean test=false;
 		if (  (   (getTime("wd").equals("Mon") && Mon==true) || 
				(getTime("wd").equals("Sun") && Sun==true) ||
				(getTime("wd").equals("Tue") && Tue==true) ||
				(getTime("wd").equals("Wed") && Wed==true) ||
				(getTime("wd").equals("Thu") && Thu==true) ||
				(getTime("wd").equals("Fri") && Fri==true) ||
				(getTime("wd").equals("Sat") && Sat==true) ) &&
				
		    (
				(Integer.parseInt(getTime("h"))>startHour && Integer.parseInt(getTime("h"))<stopHour)
				||
				(Integer.parseInt(getTime("h"))==startHour && Integer.parseInt(getTime("m"))>=startMinute)
				||
				(Integer.parseInt(getTime("h"))==stopHour && Integer.parseInt(getTime("m"))<=stopMinute)
	        )
 				
		  )	{
 		    test=true;	 		    
		}
 		return test;
 	}
 	
 	public static void writeOut(String field, String value,Context context) throws JSONException { 		
 		if(chkfile.canRead()) 			
 	        chkfile.makeDir(".retailmode"); 		
 		else
 		    return; 		
 		
	    if(jsonArray==null)
	    	jsonArray=new JSONArray().put((JSONObject)new JSONObject());
	    
		for (int i = 0; i < jsonArray.length(); i++) {
			jsonArray.getJSONObject(i).put(field, value);						    
		}
		chkfile.write(".retailmode/", "data.mem", jsonArray.toString());
		Toast.makeText(context,"Daten gespeichert", 
                Toast.LENGTH_SHORT).show(); 	
	}
}