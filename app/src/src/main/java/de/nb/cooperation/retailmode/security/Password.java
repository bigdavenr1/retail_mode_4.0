package de.nb.cooperation.retailmode.security;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.nb.cooperation.retailmode.R;
import de.nb.cooperation.retailmode.service.DataHandler;


public class Password extends Activity {
	private JSONArray jsonArray=null;
    final static DataHandler dataHandler = new DataHandler(); 

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		 try {
	        	jsonArray=dataHandler.readInfos();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		setContentView(R.layout.pw);

    	final EditText pw = (EditText)findViewById(R.id.editText1);
		final EditText pwrepeat = (EditText)findViewById(R.id.editText2);
		final Button pwOk = (Button)findViewById(R.id.button1);

		pwOk.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(String.valueOf(pw.getText()).equals(String.valueOf(pwrepeat.getText()))) {
		    		if(String.valueOf(pw.getText()).length()>=4)
						try {
							try {
								dataHandler.writeOut("pw",de.nb.cooperation.retailmode.security.AdminLogin.getEncryptedData(String.valueOf(pw.getText())),Password.this);
								startActivity(new Intent(Password.this,Settings.class));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (GeneralSecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					else
		    			Toast.makeText(Password.this,"Das Passwort muss mindestens 4 Zeichen lang sein!", 
			                    Toast.LENGTH_LONG).show();			    				    		
		        }
		    	else
		    		Toast.makeText(Password.this,"Passwort und Passwortwiederholung stimmen nicht überein!", 
		                    Toast.LENGTH_LONG).show();		    				    	
		    	pw.setText("");
	    		pwrepeat.setText("");				
			}
		});
	}		
}
