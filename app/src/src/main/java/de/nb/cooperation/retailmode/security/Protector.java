package de.nb.cooperation.retailmode.security;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import android.app.AppOpsManager;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.util.Log;
import de.nb.cooperation.retailmode.RetailMode;

public class Protector extends Service {

    private static final String TAG;
    private static final long DEFAULT_THREAD_SLEEP_TIME_MS = 500;
    private static String sCurrentProtectedPackageName;
    private static boolean sIsProtectionUnlocked;
    private static boolean sIsProtectorInitialized;
    private static String sLastPackageName;
    private static ArrayList<String> sProtectedPackageNamesList;
    private static String sProtectedResourcePrefix;
    private static boolean sRunProtectorFlag;
    private static Context sContext;
    public static boolean runGranted=false;

    static class C00881 implements Runnable {
        C00881() {}

        public void run() {
            while (Protector.sRunProtectorFlag) {
                Protector.sleepCurrentThread(Protector.DEFAULT_THREAD_SLEEP_TIME_MS);               
                String packageName = Protector.getCurrentPackageName();                
                if (!(packageName == null ) && !Protector.runGranted) {             
                    Protector.sLastPackageName = packageName;
                    if (Protector.sProtectedPackageNamesList.contains(packageName)) {
                        Protector.sCurrentProtectedPackageName = packageName;                        
						sContext.sendBroadcast(new Intent("abc"));                                                
                        Intent i = new Intent();
    		            i.setClassName("de.nb.cooperation.retailmode", "de.nb.cooperation.retailmode.RetailMode");
    		            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		            
    		            sContext.startActivity(i);   
    			        Intent intent1 = new Intent(sContext,RetailMode.class);			    
    			        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);       			         			                			       
                    }
                }                
            }
        }
    }
   
    static {
        TAG = Protector.class.getSimpleName();
        sRunProtectorFlag = false;
        sContext = null;
        sProtectedPackageNamesList = null;
        sIsProtectionUnlocked = false;
    }

    public static void init(Context context, Class protectorActivityClass) {
    	sContext = context;
        sProtectedPackageNamesList = getProtectedPackageNamesList();
        sIsProtectorInitialized = true;
    }

    private static ArrayList<String> getProtectedPackageNamesList() {
        ArrayList<String> packageNamesToProtect = new ArrayList();
        Iterator it = getIntentsToProtectList().iterator();
        while (it.hasNext()) {
            addPackageNamesForIntent((Intent) it.next(), packageNamesToProtect);
        }
        addPackageNamesFromResource(packageNamesToProtect);
        packageNamesToProtect.add("com.lge.settings.easy");
        return packageNamesToProtect;
    }

    private static ArrayList<Intent> getIntentsToProtectList() {
        ArrayList<Intent> intentsToProtectList = new ArrayList();
        intentsToProtectList.add(new Intent("com.lge.settings.easy"));
        intentsToProtectList.add(new Intent("android.settings.ACCESSIBILITY_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.ADD_ACCOUNT_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.AIRPLANE_MODE_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.APN_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.INPUT_METHOD_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.INPUT_METHOD_SUBTYPE_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.INTERNAL_STORAGE_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.LOCALE_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.MANAGE_ALL_APPLICATIONS_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.MANAGE_APPLICATIONS_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.MEMORY_CARD_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.NETWORK_OPERATOR_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.APPLICATION_DEVELOPMENT_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.APPLICATION_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.BLUETOOTH_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.CAPTIONING_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.DATA_ROAMING_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.DATE_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.DEVICE_INFO_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.DISPLAY_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.DREAM_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.SECURITY_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.SOUND_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.SYNC_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.USER_DICTIONARY_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.WIFI_IP_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.WIFI_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.WIRELESS_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.NFCSHARING_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.NFC_PAYMENT_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.NFC_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.ACTION_PRINT_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.PRIVACY_SETTINGS"));
        intentsToProtectList.add(new Intent("android.settings.QUICK_LAUNCH_SETTINGS"));
        intentsToProtectList.add(new Intent("android.search.action.SEARCH_SETTINGS"));
        return intentsToProtectList;
    }

    private static void addPackageNamesForIntent(Intent intent, ArrayList<String> packageNameList) {
        for (ResolveInfo resolveInfo : sContext.getPackageManager().queryIntentActivities(intent, AccessibilityNodeInfoCompat.ACTION_CUT)) {
            String packageNameToProtect = resolveInfo.activityInfo.packageName;
            if (!packageNameList.contains(packageNameToProtect)) {
                packageNameList.add(packageNameToProtect);
            }
        }
    }

    private static void addPackageNamesFromResource(ArrayList<String> packageNameList) {
        addStringsFromResource(sProtectedResourcePrefix, packageNameList);
    }

    private static void addStringsFromResource(String resourcePrefix, ArrayList<String> stringList) {
        int postfix = 0;
        while (true) {
            int idString = sContext.getResources().getIdentifier(resourcePrefix + postfix, "string", sContext.getPackageName());
            if (idString != 0) {
                String resourceString = sContext.getString(idString).trim();
                if (!stringList.contains(resourceString)) {
                    stringList.add(resourceString);
                }
                postfix++;
            } else {
                return;
            }
        }
    }


    public static synchronized boolean startProtector() {
        boolean z = true;
        synchronized (Protector.class) {
            if (!sIsProtectorInitialized || sRunProtectorFlag) {
                z = false;
            } else {
                Log.d(TAG, "starting protector");
                sRunProtectorFlag = true;
                new Thread(getProtectorRunnable()).start();
            }
        }
        return z;
    }

    public static synchronized void stopProtector() {
        synchronized (Protector.class) {
            sRunProtectorFlag = false;
        }
    }

    public static Runnable getProtectorRunnable() {
        return new C00881();
    }

    private static String getCurrentPackageName() {
               
    	AppOpsManager appOps = (AppOpsManager) sContext
    	        .getSystemService(Context.APP_OPS_SERVICE);
    	int mode = appOps.checkOpNoThrow("android:get_usage_stats", 
    	        android.os.Process.myUid(), sContext.getPackageName());
    	boolean granted = mode == AppOpsManager.MODE_ALLOWED;
    	
    	if(granted==false && runGranted==false) {
    		runGranted = true;
    		Intent usageAccessIntent = new Intent( Settings.ACTION_USAGE_ACCESS_SETTINGS );
    		usageAccessIntent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
    		usageAccessIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    		sContext.startActivity( usageAccessIntent );
    		usageAccessIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    	}
    	else if(granted==true)    	
    		runGranted = false;

 
    	String topPackageName ;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { 
      
            UsageStatsManager mUsageStatsManager = (UsageStatsManager)sContext.getSystemService(Context.USAGE_STATS_SERVICE);                       
            long time = System.currentTimeMillis(); 
            List<UsageStats> stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000*10, time);    
            if(stats != null) {           	
                SortedMap<Long,UsageStats> mySortedMap = new TreeMap<Long,UsageStats>();
                for (UsageStats usageStats : stats) {
                    mySortedMap.put(usageStats.getLastTimeUsed(),usageStats);
                    
                }                    
                if(!mySortedMap.isEmpty()) {
                    topPackageName =  mySortedMap.get(mySortedMap.lastKey()).getPackageName();      
                    return topPackageName;
                }                                       
            }
        }
        return null;        
    }

    private static boolean sleepCurrentThread(long millisec) {
        try {
            Thread.sleep(millisec);
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}