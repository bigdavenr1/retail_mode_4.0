package de.nb.cooperation.retailmode.security;

import org.json.JSONArray;
import org.json.JSONException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TimePicker;
import android.widget.Toast;
import de.nb.cooperation.retailmode.R;
import de.nb.cooperation.retailmode.service.DataHandler;
import de.nb.cooperation.retailmode.service.RetailModeService;
import de.nb.cooperation.retailmode.service.checkFileClass;

public class Settings extends Activity {
	final static DataHandler dataHandler = new DataHandler(); 
	
	private JSONArray jsonArray=null;
	
	// Neues Prüfobjekt erzeugen
    final static checkFileClass chkfile = new checkFileClass();
	
	protected static final String TAG = RetailModeService.class.getSimpleName();
	
    @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	finish();
    }    

    @Override
    protected void onCreate(Bundle savedInstanceState) {    	
    	// TODO Auto-generated method stub
    	super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);			
		
		// An Ausschalten des Services
		CheckBox onOff = (CheckBox)findViewById(R.id.radioButton1);
		Button saveStandBy =(Button)findViewById(R.id.button2);
		Button changePw =(Button)findViewById(R.id.button4);		
		
		final TimePicker von=(TimePicker)findViewById(R.id.timePicker1);
		final TimePicker bis=(TimePicker)findViewById(R.id.timePicker2);
		
		CheckBox isMon = (CheckBox)findViewById(R.id.checkBox1);
		CheckBox isTue = (CheckBox)findViewById(R.id.checkBox2);
		CheckBox isWed = (CheckBox)findViewById(R.id.checkBox3);
		CheckBox isThu = (CheckBox)findViewById(R.id.checkBox4);
		CheckBox isFri = (CheckBox)findViewById(R.id.checkBox5);
		CheckBox isSat = (CheckBox)findViewById(R.id.checkBox6);
		CheckBox isSun = (CheckBox)findViewById(R.id.checkBox7);
        
        try {
        	dataHandler.readInfos();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Zeit setzen im Timepicker		
		von.setIs24HourView(DateFormat.is24HourFormat(this));
		von.setCurrentHour(dataHandler.startHour);
		von.setCurrentMinute(dataHandler.startMinute);
		
		bis.setIs24HourView(DateFormat.is24HourFormat(this));
		bis.setCurrentHour(dataHandler.stopHour);
		bis.setCurrentMinute(dataHandler.stopMinute);

		// In View die gewählten Tage anzeigen		
		isMon.setChecked(dataHandler.Mon);
		isTue.setChecked(dataHandler.Tue);
		isWed.setChecked(dataHandler.Wed);
		isThu.setChecked(dataHandler.Thu);
		isFri.setChecked(dataHandler.Fri);
		isSat.setChecked(dataHandler.Sat);
		isSun.setChecked(dataHandler.Sun);
		
		// Funktionalität zum Stoppen des RetailModes
		onOff.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton onOff, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked) 
					startRetailModeService();									
				else 										
					stopRetailModeService();				
			}
		});    			
		isMon.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton isMon, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					try {
						dataHandler.writeOut("Mon","1",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						dataHandler.writeOut("Mon","0",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
			}
		});
		isTue.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton isTue, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					try {
						dataHandler.writeOut("Tue","1",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						dataHandler.writeOut("Tue","0",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
			}
		});
		isWed.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton isWed, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					try {
						dataHandler.writeOut("Wed","1",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						dataHandler.writeOut("Wed","0",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
			}
		});
		isThu.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton isThu, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					try {
						dataHandler.writeOut("Thu","1",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						dataHandler.writeOut("Thu","0",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
			}
		});
		isFri.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton isfri, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					try {
						dataHandler.writeOut("Fri","1",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						dataHandler.writeOut("Fri","0",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
			}
		});
		isSat.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton isSat, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					try {
						dataHandler.writeOut("Sat","1",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						dataHandler.writeOut("Sat","0",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
			}
		});
		isSun.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton isSun, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					try {
						dataHandler.writeOut("Sun","1",Settings.this);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				else
					try {
						dataHandler.writeOut("Sun","0",Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
			}
		});		
		von.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener(){
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				if(von.getCurrentHour()<bis.getCurrentHour() || 
				   ( von.getCurrentHour() == bis.getCurrentHour() && 
				   von.getCurrentMinute()< bis.getCurrentMinute() )){
					// TODO Auto-generated method stub				
					try {
						dataHandler.writeOut("vonH",String.valueOf(von.getCurrentHour()),Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						dataHandler.writeOut("vonM",String.valueOf(von.getCurrentMinute()),Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
				}
				else{					
					 Toast.makeText(Settings.this,"Die Startzeit muss VOR der Endzeit liegen!", 
			                 Toast.LENGTH_LONG).show();
				}				
			} 
		});		
		bis.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener(){
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				if(von.getCurrentHour()<bis.getCurrentHour() || 
						   ( von.getCurrentHour() == bis.getCurrentHour() && 
						   von.getCurrentMinute()< bis.getCurrentMinute() )){
					// TODO Auto-generated method stub				
					try {
						dataHandler.writeOut("bisH",String.valueOf(bis.getCurrentHour()),Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						dataHandler.writeOut("bisM",String.valueOf(bis.getCurrentMinute()),Settings.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else{					
					 Toast.makeText(Settings.this,"Die Startzeit muss VOR der Endzeit liegen!", 
			                 Toast.LENGTH_LONG).show();
				}
			} 
		});
				
		// Funktionalität zum Stoppen des RetailModes
		saveStandBy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Settings.this,SetTimeout.class));
			}				
		});
		
		// Funktionalität zum Stoppen des RetailModes
	    changePw.setOnClickListener(new OnClickListener() {
		  
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Settings.this,Password.class));
		    }				
		});    			
    }    
    
    /**
     * Methode zum Abschalten des Services
     */
    private void stopRetailModeService() {
    	Intent intent=new Intent(this, RetailModeService.class);
    	
    	stopService(intent);
    	Toast.makeText(Settings.this,"Service und Schutz sind abgeschaltet", 
                Toast.LENGTH_LONG).show();
    }
    
    /** 
     * Methode zum Einschalten des Services
     */
    private void startRetailModeService(){
    	 startService(new Intent(this, RetailModeService.class));
    	 Toast.makeText(Settings.this,"Service und Schutz sind wieder aktiviert", 
                 Toast.LENGTH_LONG).show();
    }     
}