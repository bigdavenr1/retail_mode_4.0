// Package
package de.nb.cooperation.retailmode.service;

import org.json.JSONException;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import de.nb.cooperation.retailmode.RetailMode;
import de.nb.cooperation.retailmode.security.Protector;

/**
 * Serviceklasse für das gerät
 * @author Stief
 *
 */
public class RetailModeService extends Service {		
	boolean handler2IsSet=false;
    final static DataHandler dataHandler = new DataHandler();    
	protected static final String TAG = RetailModeService.class.getSimpleName();
	private static RetailModeService instance;
	protected static int batterylevel;
    protected static int timoetoshowtoast=1;
	private PowerManager.WakeLock wakeLock;
	public static int timer=0;
	public int minmalAkku=15;
	private Boolean receiverunregistered=true;
	BroadcastReceiver onScreenOffReceiver;
	BroadcastReceiver onSystemOffReceiver;
	public static Boolean videoIsPlaying=false;
	
	private final BroadcastReceiver toast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
       	 Toast.makeText(RetailModeService.this,"Funktion gesperrt!", 
	                Toast.LENGTH_LONG).show();
		     RetailModeService.timoetoshowtoast=1;                                 
        }
    };
    
    private final BroadcastReceiver VideoPlay = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
		     RetailModeService.videoIsPlaying=false;
		     RetailModeService.timer=0;
        }
    };
	
	private Handler handler2 = new Handler();
	private Runnable timeOut =new Runnable() {
		@Override
		public void run() {
			if(RetailModeService.batterylevel>minmalAkku) {
				 handler2.postDelayed(this, 900);
				 timer++;
				 if(dataHandler.standByStart==timer && Protector.runGranted!=true){
					 if(videoIsPlaying==false){
					    startNewIntent();
					    videoIsPlaying=true;
					 }
					 timer=0;				
				 }
				 else if(Protector.runGranted==true) {
					 timer=0;
					 
			    	Intent usageAccessIntent = new Intent( Settings.ACTION_USAGE_ACCESS_SETTINGS );
			    	usageAccessIntent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
			    	usageAccessIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			    	startActivity( usageAccessIntent );
			    	usageAccessIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				 }
				 Log.d("adp",String.valueOf(timer)+String.valueOf(videoIsPlaying));
		    }	
		}
	};	
	
	// Timer
	private Handler handler = new Handler();
	private Runnable videoStarter= new Runnable() {		
		
	    WakeLock mWakeLock;

		@Override
	    public void run() {		
			Log.d("adp",String.valueOf(dataHandler.getState()));
	        /* and here comes the "trick" */
	        handler.postDelayed(this, 10000);	     
	        try {
			    dataHandler.readInfos();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
	        if(dataHandler.getState()==false ){	
    		    RetailModeService.timoetoshowtoast=1;
    		    Protector.stopProtector();
    		    unregisterAllReceiver();			  				  	      
		  	    receiverunregistered=true;
		  	    sendBroadcast(new Intent("xyz"));
		  	    if(handler2IsSet==true) {
		  	    	handler2.removeCallbacks(timeOut);	
		  	    	handler2IsSet=false;  	    	   	
    		    }
		  	}
	        else {	
        	   if(RetailModeService.batterylevel<=minmalAkku) {
	    		    if(RetailModeService.timoetoshowtoast%30==0){
		    		     Toast.makeText(RetailModeService.this,"Achtung Battery laden. RetailMode gesperrt", 
		    	                        Toast.LENGTH_LONG).show();
		    		     RetailModeService.timoetoshowtoast=1;
	    		    }
	    		    RetailModeService.timoetoshowtoast++;	    		 
                }
	    	    RetailModeService.timoetoshowtoast=1;
	    	    if(receiverunregistered==true) {	    	      
	    		    initGlobalTouchListener();
	    		    initAndStartProtector();	    		  
	                registerKioskModeScreenOffReceiver();
  	                registerReceiver(toast, new IntentFilter("abc"));
   	                startNewIntent();
   	                videoIsPlaying=true;
                }
	    	    if(dataHandler.standByStart>0 && RetailModeService.batterylevel>minmalAkku && handler2IsSet==false) {	    	    	
	    	    	final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
	    	        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
	    	        this.mWakeLock.acquire();
	    	        handler2.postDelayed(timeOut, 1000);
	    	        handler2IsSet = true;	    	        
	    	    }
	    	    else {
	    	    	if(handler2IsSet==true)	{
	    	    		handler2.removeCallbacks(timeOut);
	    	    		final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);		    	        
		    	        this.mWakeLock.acquire();		    	       
		    	        handler2IsSet = false;
	    	    	}
	    	    }
	        }
	    }
    };
	
	private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
	    public boolean onTouch(View anonView, MotionEvent anonEvent) {	    		         
	        timer=0;
	        videoIsPlaying=false;
	        return false;
	    }
	};
	private WakeLock mWakeLock;	   
	    
	public void onCreate() {		
		instance = this;
        super.onCreate();
		registerReceiver(toast, new IntentFilter("abc"));
        try {
			dataHandler.readInfos();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
        startTimer();
        
        BroadcastReceiver batteryReceiver = new BroadcastReceiver(){
			@Override
			public void onReceive(Context context, Intent intent) {
				
				context.unregisterReceiver(this);
                int rawlevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int level = -1;
                if (rawlevel >= 0 && scale > 0) {
                    level = (rawlevel * 100) / scale;
                }
                RetailModeService.batterylevel=level;
                Intent i = new Intent();
	            i.setClassName("de.nb.cooperation.retailmode", "de.nb.cooperation.retailmode.RetailMode");
	            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		            
			}
		};
		IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
	    registerReceiver(batteryReceiver, batteryLevelFilter);
    }
	
	private void initAndStartProtector() {
	        Protector.init(getApplicationContext(), RetailMode.class);
	        Protector.startProtector();
	}
	/**
	 * Starttimer Methode
	 */
	private void startTimer() {
		handler.postDelayed(videoStarter, 1000);		
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	/**
	 * Methode zum Erzeugen eines Touchlisteners über den gesamten Bildschirm. 
	 * Erzeugt ein globales View
	 * @return void
	 */
	private void initGlobalTouchListener() {
        View localView = new View(this);
        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams(-2, -2, 2007, 262216, -3);
        localLayoutParams.gravity = 51;
        localLayoutParams.x = 0;
        localLayoutParams.y = 0;
        localLayoutParams.width = 0;
        localLayoutParams.height = 0;
        localView.setOnTouchListener(this.onTouchListener);
        ((WindowManager)getSystemService(WINDOW_SERVICE)).addView(localView, localLayoutParams);
    }
	
	private void registerKioskModeScreenOffReceiver() {		
	    receiverunregistered=false;
	    // register screen off receiver
	    final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);	    
	    final IntentFilter filter2 = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);	    
	    
	    // OnscreenReceiver ScreenOff
	    onScreenOffReceiver = new BroadcastReceiver(){
			@Override
	        public void onReceive(Context context, Intent intent) {			    	
			    Log.d(TAG,intent.getAction());
			    String str="android.intent.action.SCREEN_OFF";
			    String action=intent.getAction();
			    boolean bool = str.equals(action);
			    if(bool == true){ 		    		
			    	startNewIntent();
			    	videoIsPlaying=true;
			    }	
			}
		};
	    registerReceiver(onScreenOffReceiver, filter);
	    
	    // OnscreenReceiver ScreenOff
	    onSystemOffReceiver = new BroadcastReceiver(){
    	    @Override
		    public void onReceive(Context context, Intent intent) {	 
    		    String str="android.intent.action.CLOSE_SYSTEM_DIALOGS";
		    	String action=intent.getAction();
		    	boolean bool = str.equals(action);
		    	if(bool == true){ 			    		
		    		setPowerManager();
                }
            }
        };
        registerReceiver(onSystemOffReceiver, filter2);
    }
	
	public PowerManager.WakeLock getWakeLock() {
	    if(wakeLock == null) {
	        // lazy loading: first call, create wakeLock via PowerManager.
	        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
	        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");
	    }
	    return wakeLock;
	}
	 
	public void onScreenStateChange(boolean paramBoolean){}
	 
	public void onBackPressed() {}	
	 
    @Override
	public void onDestroy() {
		super.onDestroy();
		unregisterAllReceiver();
		handler.removeCallbacks(videoStarter);
		handler2.removeCallbacks(timeOut);		
		videoStarter=null;
		timeOut=null;
		handler=null;
		handler2=null;
		Protector.stopProtector();
	}	 	
	
	private void unregisterAllReceiver() {
		if(receiverunregistered!=true) {
		    unregisterReceiver(onScreenOffReceiver );
		    unregisterReceiver(onSystemOffReceiver );
		    unregisterReceiver(toast);
		}
	}
	
	public void setPowerManager(){
	    PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
	    WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");
		if (wakeLock.isHeld()) {
		    wakeLock.release(); 
		}
		wakeLock.acquire();
        wakeLock.release();
	}
	
	public void startNewIntent(){
		if(RetailModeService.batterylevel>minmalAkku) {
			Intent i = new Intent();
	        i.setClassName("de.nb.cooperation.retailmode", "de.nb.cooperation.retailmode.RetailMode");
	        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		        
	        startActivity(i);           
	        Log.d(TAG,"Display wurde gesperrt");
	        Intent intent1 = new Intent(getApplicationContext(),RetailMode.class);			    
	        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);        			      
	        setPowerManager();
		}
	}
}